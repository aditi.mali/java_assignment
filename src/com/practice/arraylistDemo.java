package com.practice;

import java.util.ArrayList;

public class arraylistDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<String> al=new ArrayList<>();
		System.out.println(al.size());
		al.add("a");
		al.add("b");
		al.add("c");
		al.add("d");
		al.add(null);
		al.add("a");
		System.out.println(al.size());//element stored in consecutive memory location

		//replace the second element with B1
		al.set(1, "B1");
		System.out.println(al);
		/*for(String s: al)
		{
			System.out.println(s);
		}*/
		al.remove("c");
		al.remove(3);
		System.out.println(al);
	}

}
