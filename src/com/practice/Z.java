package com.practice;

class X {

	
	public void methodx()
	{
		System.out.println("class x method");
	}
}

class Y extends X{
	
	public void methodY()
	{
		System.out.println("class Y method");
	}
}
public class Z  extends Y
{
	
	public void methodz()
	{
		System.out.println("class z method");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Z obj= new Z();
		obj.methodx();
		obj.methodY();
		obj.methodz();
		
	}

}
