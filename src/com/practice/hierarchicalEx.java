package com.practice;

class parent3
{
	public void method1()
	{
		System.out.println("parent class method");
	}
}

class child1 extends parent3
{
	public void method2()
	{
		System.out.println("child1 class method");
	}
}

class child2 extends parent3
{
	public void method3()
	{
		System.out.println("child2 class method");
	}
}


public class hierarchicalEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		parent3 p= new parent3();
		p.method1();
		System.out.println("-----------------------");
		
		child1 c1=new child1();
		c1.method1();
		c1.method2();
		System.out.println("-----------------------");
		
		child2 c2=new child2();
		c2.method1();
		c2.method3();

	}

}
