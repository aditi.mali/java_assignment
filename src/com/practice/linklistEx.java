package com.practice;

import java.util.LinkedList;

public class linklistEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> list = new LinkedList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add(null);
		list.add("a");
		System.out.println(list);
		
		list.add(1,"a1");
		System.out.println(list);
		list.addFirst("aaaa");
		list.addLast("xxxx");
		System.out.println(list);
		list.remove("d");
	
	}

}
//a points to b, b points to c, c points to d 
//a points to a1, a1 points b, b points to c, c points to d 