package com.practice;


class A 
{
	public void display() 
		{
			System.out.println("One");
		}
}
//inheritance
class B extends A 
{
     @Override
	public void display() 
     {
    	 System.out.println("Two");
     }
     public int add(int x, int y) 
     {
    	return x+y;
     }
     //Overload
     public double add(double x,double y) 
     {
    	 return x+y;

     }
}

class C 
{
	private String name;
		public String getName() 
		{
			return name;
		}
		public void setName(String newName) 
		{
			name = newName;
		}

}
//abstraction
abstract class TwoWheeler 
{
	public abstract void run();
}
class Honda extends TwoWheeler
{
	public void run()
	{
		System.out.println("bike is Running..");
	}
}
public class allOoppsEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		A a=new A();

		a.display();

		B b=new B();

		b.display();

		System.out.println(b.add(4,2));

		System.out.println(b.add(5.,2.)); //polymorphism

		C encap = new C();

		encap.setName("ADITI");

		System.out.print("Name : " + encap.getName() );

		TwoWheeler test = new Honda();

		test.run();

	}

}
