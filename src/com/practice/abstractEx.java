package com.practice;

// an abstract class is a class its  consists of both abstract and non abstract method
//non abstract method is not compulsory..
//an absrtact method is a method without a body, it is just declared.
//abstract class cant be instantiated,that is we can not create object of that class
// a child class can implement all the abstract methods present in parent
//what happen if child does not implement abstract method?
public abstract  class abstractEx {

	
	public abstract void employee();
	public void d1()
	{
		
	}
	
}
