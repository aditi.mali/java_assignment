package com.practice;

import java.util.HashMap;

public class hashmapEx {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub

		 HashMap <Integer,String> map= new HashMap<Integer, String>();
	        
	        //Add element
	        map.put(1, "aditi");
	        map.put(2, "tina");
	        map.put(3, "Meena");
	        map.put(4, "seema");
	        System.out.println(map);
	        
	        //Remove element on key based
	        map.remove(2);
	        System.out.println("After remove value of 2nd key:-"+map);
	        
	        //
	        map.put(2,"eeeee");
	        System.out.println("After  adding value at 2nd key:-"+map);
	        System.out.println(map);
	    
	        map.replace(4, "M");
	        System.out.println("After replacing value of 4th key:-"+map);
		
	}

}
